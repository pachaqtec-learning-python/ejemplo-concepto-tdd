import unittest
import json

from main import app

BASE_URL = "http://localhost:8086/api/movie"
GOOD_URL = f"{BASE_URL}/2"
BAD_URL = f"{BASE_URL}/9"

class TestMovies(unittest.TestCase):

    def setUp(self):
        self.request = app.test_client()
        self.request.testing = True

    def test_create(self):
        payload = json.dumps({
            "titulo":"TERMINATOR",
            "director": "STEVEN SPIELBERG",
            "descripcion": "Pelicula sobre bla bla bla bla",
            "genero": "Ciencia Ficción",
            "año": "1999"
        })
        response = self.request.post(BASE_URL,  headers={"Content-Type": "application/json"}, data=payload)
        self.assertEqual(201, response.status_code)

        dataJson = response.get_json()
        self.assertEqual("TERMINATOR", dataJson["titulo"])

    def test_readALL(self):
        response = self.request.get(BASE_URL)
        self.assertEqual(200, response.status_code)

        dataJson = response.get_json()
        self.assertEqual(3, len(dataJson))

    def test_readOne(self):
        response = self.request.get(GOOD_URL)
        self.assertEqual(200, response.status_code)

        dataJson = response.get_json()
        self.assertEqual("AVATAR", dataJson["titulo"])

    def test_readOneError(self):
        response = self.request.get(BAD_URL)
        self.assertEqual(404, response.status_code)

        dataJson = response.get_json()
        message = dataJson["message"].split(".")
        self.assertEqual("NOT FOUND", message[0])

    def test_update(self):
        response = self.request.put(GOOD_URL)
        self.assertEqual(204, response.status_code)

    def test_updateError(self):
        response = self.request.put(BAD_URL)
        self.assertEqual(200, response.status_code)

        dataJson = response.get_json()
        self.assertEqual("EL ID QUE INTENTA ACTUALIZAR NO EXISTE", dataJson["message"])

    def test_delete(self):
        response = self.request.delete(GOOD_URL)
        self.assertEqual(200, response.status_code)

        dataJson = response.get_json()
        self.assertEqual("RECURSO ELIMINADO", dataJson["message"])

    def test_deleteError(self):
        response = self.request.delete(BAD_URL)
        self.assertEqual(200, response.status_code)

        dataJson = response.get_json()
        self.assertEqual("EL ID QUE INTENTA ELIMINAR NO EXISTE", dataJson["message"])

if __name__ == "__main__":
    unittest.main()