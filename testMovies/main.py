from flask import Flask, abort, request
from flask_restx import Api, Resource

# Configuración Inicial Flask
app = Flask(__name__)
api = Api(app)


# Data Harcodeada de Uusuarios
listMovies = [
    {
        "id": 1,
        "titulo":"TERMINATOR 03",
        "director": "STEVEN SPIELBERG",
        "descripcion": "Pelicula sobre bla bla bla bla",
        "genero": "Ciencia Ficción",
        "año": "1999"
    },{
        "id": 2,
        "titulo":"AVATAR",
        "director": "STEVEN SPIELBERG",
        "descripcion": "Pelicula sobre bla bla bla bla",
        "genero": "Ciencia Ficción",
        "año": "1999"
    },{
        "id": 3,
        "titulo":"TRANSFORMES",
        "director": "STEVEN SPIELBERG",
        "descripcion": "Pelicula sobre bla bla bla bla",
        "genero": "Ciencia Ficción",
        "año": "1999"
    },
]

# Método Reusable para según el ID encontrar el usuario en la Lista
def searchMovie(id):
      return [movie for movie in listMovies if movie["id"] == id]

# get, put, delete
class MovieController(Resource):
    def get(self, id):
        movie = searchMovie(id)
        if not movie: abort(404, "NOT FOUND")
        return movie[0], 200

    def put(self, id):
        movie = searchMovie(id)
        if not movie: return {"message": "EL ID QUE INTENTA ACTUALIZAR NO EXISTE"}, 200
        return "ok", 204

    def delete(self, id):
        movie = searchMovie(id)
        if not movie: return {"message": "EL ID QUE INTENTA ELIMINAR NO EXISTE"}, 200
        return {"message": "RECURSO ELIMINADO"}, 200

# post, get
class MoviePostController(Resource):
    def get(self):
        return listMovies, 200

    def post(self):
        dataJson = request.get_json()
        return dataJson, 201

movie = api.namespace('api', description='API MOVIE')
movie.add_resource(MoviePostController, '/movie')
movie.add_resource(MovieController, '/movie/<int:id>')

if __name__ == "__main__":
	app.run(port=8086, debug=True)


