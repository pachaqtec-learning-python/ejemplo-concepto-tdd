import unittest

from calculator import Calculator

# https://www.hektorprofe.net/tutorial/ejemplo-muy-facil-tdd-python
# Creamos la clase Heredando de TestCase
class TestCalculator(unittest.TestCase):

    # Los metodos deben de tener la letra "test_XXXX", para que sea reconocido como método a evaluar
    def test_valueInitial(self):
        calculator = Calculator()
        self.assertEqual(0, calculator.value)

    def test_sumarValores(self):
        calculator = Calculator()
        self.assertEqual(6, calculator.sumar(5, 1))

    def test_restarValores(self):
        calculator = Calculator()
        self.assertEqual(4, calculator.restar(7, 3))

