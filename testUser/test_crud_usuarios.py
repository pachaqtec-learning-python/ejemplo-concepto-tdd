import unittest
from copy import deepcopy

from main import listUsuarios, app

BASE_URL = "http://localhost:8080/users"
GOOD_URL = f"{BASE_URL}/3"
BAD_URL = f"{BASE_URL}/9"


class TestUsuario(unittest.TestCase):

    # Este método se ejecuta pre ejecución de cada test.
    def setUp(self):
        # self.backupUsuarios = deepcopy(listUsuarios)
        self.request = app.test_client()
        self.request.testing = True


    # Este método se ejecuta post ejecución de cada test.
    def tearDown(self):
        # listUsuarios = self.backupUsuarios
        pass

    # Status Code =  200
    def test_getALLUser(self):
        response = self.request.get(BASE_URL)
        data = response.get_json()
        self.assertEqual(200, response.status_code)
        self.assertEqual(3, len(data["users"]))

    # Status Code =  200
    def test_getOneUser(self):
        # GOOD
        response = self.request.get(GOOD_URL)
        data = response.get_json()
        self.assertEqual(200, response.status_code)
        self.assertEqual("cgonzales", data["user"][0]["username"])

        # BAD
        response = self.request.get(BAD_URL)
        data = response.get_json()
        self.assertEqual(404, response.status_code)
        self.assertTrue(True if "error" in data else False)


    # # Status Code =  200
    # def test_deleteUser(self):
    #     print("test_deleteUser")

    # # Status Code =  200
    # def test_updateUser(self):
    #     print("test_updateUser")


if __name__ == "__main__":
    unittest.main()
