from flask import Flask, jsonify, abort, make_response
app = Flask(__name__)

# Data Harcodeada de Uusuarios
listUsuarios = [
    { 'id': 1, 'username': 'acornejo',  'edad': 33 },
    { 'id': 2, 'username': 'rpineda',   'edad': 23 },
    { 'id': 3, 'username': 'cgonzales', 'edad': 30 },
]

# Metodo Reusable para segun el ID encontrar el usuario en la Lista
def searchUser(id):
      return [user for user in listUsuarios if user["id"] == id]

# Controlar o personalizar el error 404
@app.errorhandler(404)
def not_found(error):
      return make_response(jsonify({ "error": "Not Found - Validate URL"}), 404)
      #return jsonify({ "error": "Not Found - Validate URL"}), 404

# Ruta para obtener los usuarios
@app.route('/users', methods=["GET"])
def listUsers():
    return jsonify({"users" : listUsuarios})

# Ruta para obtener un usuario segun el ID
@app.route('/users/<int:id>', methods=["GET"])
def getUser(id):
    user =  searchUser(id)
    if not user : abort(404)
    return jsonify({"user": user})

if __name__ == '__main__':
  app.run(port=8080, debug=True)
